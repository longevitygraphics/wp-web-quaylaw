// Windows Ready Handler
 	
(function($) {
    $(document).ready(function(){
        var $practiceWrap  = $('#practicewrap');
        var $practice = $('#practicewrap .content-column');

        $practice.hover(
            function(){
                $(this).children('.practice').addClass('feature');
            }, function() {
                $(this).children('.practice').removeClass('feature');
            }
        )

    });
}(jQuery));