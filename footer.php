<?php
/**
 * The template for displaying the footer.
 *
 * Contains the closing of the id=main div and all content
 * after. Calls sidebar-footer.php for bottom widgets.
 *
 * @package WordPress
 * @subpackage Twenty_Ten
 * @since Twenty Ten 1.0
 */
?>



<footer class="fullwidth" role="contentinfo">

  <div id="loco">
  <div class="container">
<?php dynamic_sidebar( 'footer-widget-area' ); ?>
 <div class="legal">
	The lawyers of Quay Law Centre represent clients in New Westminster, Vancouver, Burnaby, Richmond, Surrey, Coquitlam, Port Coquitlam, Port Moody, White Rock, Langley, Pitt Meadows, Maple Ridge,
the Fraser Valley, the Lower Mainland and throughout British Columbia (B.C.), as well as residents of Canada, the United States and international clients.

*The Quay Law Centre is an association of Personal Law Corporations, it is not a partnership.
	 </div>

	</div>
	</div>

	<div id="menuwrap">
	<div class="container">
<div class="footmenu left white"><?php wp_nav_menu( array( 'theme_location' => 'footer' ) ); ?></div>
		<div class="footconsult right"><a href="tel:604-527-1161" class="btn dkblue btblueback">CALL NOW</a></div>
	</div>
	</div>

<div id="copyright">
<div class="container">
<div class="sitecopy left">
<?php
	get_sidebar( 'footer' );
	$date = getdate();
	$year = $date['year'];
?>
				&copy; <?php echo("$year"); ?> <a href="http://pview.findlaw.com/cmd/view?wld_id=4393127&amp;pid=1" target="_blank"><?php bloginfo( 'name' ); ?></a> All Rights Reserved. | <a href="/disclaimer">Disclaimer</a> | <a href="/privacy-policy">Privacy Policy</a>
</div>
<div class="longevity right">
<a target="_blank" href="http://www.longevitygraphics.com">Website Design</a> by <a target="_blank" href="http://www.longevitygraphics.com">Longevity Graphics</a>
</div>
	</div>
	</div>

</footer>

<?php wp_footer(); ?>
</body>
</html>
