<?php
/**
* Front Page
 */

get_header(); ?>

<section class="content">
		<main id="content" role="main" class="one-column">

			<?php
			/* Run the loop to output the page.
			 * If you want to overload this in a child theme then include a file
			 * called loop-page.php and that will be used instead.
			 */
			 get_template_part( 'loop', 'page' );
			?>

		</main>
</section>

<?php get_footer(); ?>
