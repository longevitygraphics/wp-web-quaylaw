<?php

function child_twentyten_widgets_init() {
	// Area 1, located at the top of the sidebar.
	register_sidebar( array(
		'name' => __( 'Sidebar Area', 'twentyten' ),
		'id' => 'primary-widget-area',
		'description' => __( 'Add widgets here to appear in your sidebar.', 'twentyten' ),
		'before_widget' => '<section id="%1$s" class="widget-container %2$s">',
		'after_widget' => '</section>',
		'before_title' => '<h4 class="widget-title">',
		'after_title' => '</h4>',
	) );
	
		register_sidebar( array(
		'name' => __( 'Blog Sidebar Area', 'twentyten' ),
		'id' => 'blog-sidebar',
		'description' => __( 'Add widgets here to appear in your sidebar.', 'twentyten' ),
		'before_widget' => '<section id="%1$s" class="widget-container %2$s">',
		'after_widget' => '</section>',
		'before_title' => '<h4 class="widget-title">',
		'after_title' => '</h4>',
	) );
	
	
	register_sidebar( array(
		'name' => __( 'Footer Area', 'twentyten' ),
		'id' => 'footer-widget-area',
		'description' => __( 'These widgets will show up on your footer', 'twentyten' ),
		'before_widget' => '<section id="%1$s" class="widget-container %2$s">',
		'after_widget' => '</section>',
		'before_title' => '<h4 class="widget-title">',
		'after_title' => '</h4>',
	) );
}


//AFTER THEME IS SET UP (FUNCTIONS.PHP IS LOADED FROM PARENT THEME) - REMOVE THE SIDEBAR WIDGETS
add_action('after_setup_theme','remove_parent_widgets');
function remove_parent_widgets() {
    remove_action( 'widgets_init', 'twentyten_widgets_init' );
}
//RE-ADD THE SIDEBAR WIDGETS WITH THE NEW CODE ADDED, DUPLICATING THE FUNCTION
add_action( 'after_setup_theme', 'child_twentyten_widgets_init' );

//ADD REALLY SIMPLE CAPTCHA BACK TO CONTACT FORM 7
add_filter( 'wpcf7_use_really_simple_captcha', '__return_true' );

//CHANGE DEFAULT THEME NAME
add_filter('default_page_template_title', function() {
    return __('One column, right sidebar', 'your_text_domain');
});

//REMOVES HENTRY FROM PAGES TO ELIMINATE ERRORS IN GOOGLE WEBMASTER TOOLS
function themeslug_remove_hentry( $classes ) {
    if ( is_page() ) {
        $classes = array_diff( $classes, array( 'hentry' ) );
    }
    return $classes;
}
add_filter( 'post_class','themeslug_remove_hentry' );

//REMOVES QUERY(?) STRINGS FROM URLS
// function _remove_script_version( $src ){
// 	$parts = explode( '?ver', $src );
// 	return $parts[0];
// }
// add_filter( 'script_loader_src', '_remove_script_version', 15, 1 );
// add_filter( 'style_loader_src', '_remove_script_version', 15, 1 );

//DEFER JQUERY PARSING USING THE HTML5 DEFER PROPERTY
// if (!(is_admin() )) {
//     function defer_parsing_of_js ( $url ) {
//         if ( FALSE === strpos( $url, '.js' ) ) return $url;
//         if ( strpos( $url, 'jquery.js' ) ) return $url;
//         // return "$url' defer ";
//         return "$url' defer onload='";
//     }
//     add_filter( 'clean_url', 'defer_parsing_of_js', 11, 1 );
// }


// This theme uses wp_nav_menu() in two locations.  
register_nav_menus( array(  
  'primary' => __( 'Primary Navigation', 'twentyten' ),  
  'footer' => __('Footer Navigation', 'twentyten')  
) );

// Limit the Archives in the Sidebar for Blog 
function my_limit_archives( $args ) {
    $args['limit'] = 15;
    return $args;
}
add_filter( 'widget_archives_args', 'my_limit_archives' );



/* Modify the read more link on the_excerpt() */
 
function et_excerpt_length($length) {
    return 220;
}
add_filter('excerpt_length', 'et_excerpt_length');
 
/* Add a link  to the end of our excerpt contained in a div for styling purposes and to break to a new line on the page.*/
 
function et_excerpt_more($more) {
    global $post;
    return '<a href="'. get_permalink($post->ID) . '"> Read more on the blog <span class="meta-nav">&rarr;</span></a>';
}
add_filter('excerpt_more', 'et_excerpt_more');

// 	// override parent theme's 'more' text for excerpts
// function child_theme_setup() {

// 	remove_filter( 'excerpt_more', 'twentyten_auto_excerpt_more' ); 
// 	remove_filter( 'get_the_excerpt', 'twentyten_custom_excerpt_more' );
// }
// add_action( 'after_setup_theme', 'child_theme_setup' );




//DONT ERASE SPAN TAGS WHEN SWITCHING TO VISUAL / TEXT MODE
function override_mce_options($initArray) {
$opts = '*[*]';
$initArray['valid_elements'] = $opts;
$initArray['extended_valid_elements'] = $opts;
return $initArray;
}
add_filter('tiny_mce_before_init', 'override_mce_options');







/**
 * Enqueue scripts and styles.
 */
function quaylawcentre_scripts() {
	
	// Fonts
	wp_enqueue_style( 'wpb-google-fonts', 'https://fonts.googleapis.com/css?family=Muli:300,400,600,700,800,900', false );

	// enqueue style
	wp_enqueue_style( 'lg-style', get_stylesheet_directory_uri() . '/style.css', [], wp_get_theme()->get('Version') );
	wp_register_script( 'slickslider', get_stylesheet_directory_uri() . "/assets/dist/js/slick.min.js", array('jquery'), false );

	// enqueue script
	wp_enqueue_script( 'lg-script', get_stylesheet_directory_uri() . '/assets/dist/js/script.min.js', array('jquery'), false  );

	if (is_front_page()) {
		wp_enqueue_script( 'slickslider' );
	}
	
}
add_action('wp_enqueue_scripts', 'quaylawcentre_scripts');

/*Remove jquery migrate js*/
function remove_jquery_migrate($scripts)
{
	if (!is_admin() && isset($scripts->registered['jquery'])) {
		$script = $scripts->registered['jquery'];

		if ($script->deps) { // Check whether the script has any dependencies
			$script->deps = array_diff($script->deps, array(
				'jquery-migrate'
			));
		}
	}
}
add_action('wp_default_scripts', 'remove_jquery_migrate');

//Remove Gutenburg css
add_action( 'wp_print_styles', 'wps_deregister_gutenberg_styles', 100 );
function wps_deregister_gutenberg_styles() {
	wp_dequeue_style( 'wp-block-library' );
}

// dequeue stylesheets and scripts conditionally
// function lg_conditional_scripts_styles() {
//     //get current url
//     global $wp;
//     $current_url = home_url( $wp->request );
//     //check if page is not /contact
//     if(!strstr( $current_url, '/contact')){  
//         wp_dequeue_script("lg-map");     
// 	}
// 	wp_dequeue_style('sb-font-awesome');
// }
// add_action('wp_enqueue_scripts', 'lg_conditional_scripts_styles', 100);





// Not sure why this is here
add_action( 'wp_footer', 'mycustom_wp_footer' );
 
function mycustom_wp_footer() {
?>
<script type="text/javascript">
document.addEventListener( 'wpcf7mailsent', function( event ) {
   location = window.location.hostname + '/thank-you/';
}, false );
</script>
<?php
}