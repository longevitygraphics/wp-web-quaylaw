<?php
/**
 * The Header for our child theme.
 *
 * Displays all of the <head> section and everything up till <div id="main">
 *
 * @package WordPress
 * @subpackage Responsive Twenty_Ten
 * @since Responsive Twenty Ten 0.1
 */
?><!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>

<meta charset="<?php bloginfo( 'charset' ); ?>" />
	<meta name="viewport" content="width=device-width, minimum-scale=1.0, maximum-scale=1.0">
	<meta name="SKYPE_TOOLBAR" content="SKYPE_TOOLBAR_PARSER_COMPATIBLE" />
	<title><?php wp_title( '|', true, 'right' ); ?></title>
	<link rel="profile" href="http://gmpg.org/xfn/11" />
	<link rel="shortcut icon" href="<?php echo get_stylesheet_directory_uri(); ?>/images/favicon.ico" />
	<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>" />
	
	<?php
		if ( is_singular() && get_option( 'thread_comments' ) )
			wp_enqueue_script( 'comment-reply' );
	?>

	<?php wp_head(); ?>

</head>

<body <?php body_class(); ?>>


<header class="fullwidth" role="banner">
	
	<div class="topbar">
		<div class="container">
			
			<a class="tollfree" href="tel:+16045271161"><span>PHONE: &nbsp;</span>604-527-1161</a>
		</div>
	</div>

	<div class="container">

		<a id="logo" href="<?php echo home_url( '/' ); ?>" title="<?php echo esc_attr( get_bloginfo( 'name', 'display' ) ); ?>" rel="home">
			<?php if(is_front_page()){?>
				<h1><?php echo esc_attr( get_bloginfo( 'name', 'display' ) ); ?></h1>
			<?php } ?>
			<img class="logo" alt="<?php echo esc_attr( get_bloginfo( 'name', 'display' ) ); ?>" src="<?php echo get_stylesheet_directory_uri(); ?>/images/quay-law.png" />
		</a>

		<nav role="navigation">
			
			<div class="skip-link screen-reader-text">
				<a href="#content" title="<?php esc_attr_e( 'Skip to content', 'twentyten' ); ?>"><?php _e( 'Skip to content', 'twentyten' ); ?></a>
			</div>
			
			<?php wp_nav_menu( array( 'theme_location' => 'primary' ) ); ?>

		</nav>

	</div>
</header>

<?php 
    global $post;
    $post_slug = $post->post_name;
    $metasliderID = 32;

	$metasliderpage = array(
		'lawyer-profiles'                                   => 1125,
		'russell-s-tretiak'                                 => 1125,
		'carol-w-hickman'                                   => 1125,
		'william-laurence-scott'                            => 1125,
		'lori-gerbig'                                       => 1125,
		'rupinder-shoker'                                   => 1125,
		'sarah-hyejung-jo'                                  => 1125,
		'marta-l-brus'                                      => 1125,
		'candice-l-hall'                                    => 1125,
		'brandon-hastings'                                  => 1125,
		'rasjovan-jovan-s-dale'                             => 1125,
		'suzanne-fleming'                                   => 1125,
		'firm-overview'                                     => 1031,
		'testimonials'                                      => 1131,
		'blog'                                              => 1116,
		'contact'                                           => 1122,
		'family-law-overview'                               => 1119,
		'client-responsibility'                             => 1119,
		'court-processes-explained'                         => 1119,
		'supreme-court-processes-and-procedures'            => 1119,
		'appeals'                                           => 1128,
		'divorce'                                           => 1128,
		'child-focused-dispute-resolution'                  => 1128,
		'spousal-support'                                   => 1128,
		'mediation-collaborative-law-and-arbitration'       => 1128,
		'family-law-mediation-and-arbitration'              => 1128,
		'agreements'                                        => 1128,
		'valuation-of-assets-including-business-valuations' => 1128,
		'complex-and-high-asset-property-division'          => 1128,
		'pension-r-r-s-p-division'                          => 1128,
		'equal-or-unequal-division-of-property'             => 1128,
		'common-law-rights-property-divisions'              => 1128,
		'same-sex-marriage-property-division'               => 1128,
		'divorce-and-small-business-ownership'              => 1128,
		'child-guardianship-and-access'                     => 1128,
		'child-support-special-and-extraordinary-expenses'  => 1128,
		'variation-and-enforcement-of-orders'               => 1128,
		'mobility-issues'                                   => 1128,
		'orders-of-no-contact-protection'                   => 1128,
		'paternity-claims'                                  => 1128,
		'third-party-rights-grandparents-and-relatives'     => 1128,
		'adoption'                                          => 1128,
		'divorce-and-bankruptcy'                            => 1128,
		'divorce-and-foreclosure'                           => 1128,
		'determination-of-income'                           => 1128,
	);

	if ( array_key_exists($post_slug, $metasliderpage) ) {
		$metasliderID = $metasliderpage[$post_slug];
	}elseif ( is_singular() || is_home() ) {
		$metasliderID = 1116;
	}
	else{
		$metasliderID = 32;
	}
?>


<?php if(is_front_page()): ?>

	<div class="bgc-brand">
		<section class="slider">
			
			<div class="slider-cont">
				<?php // echo do_shortcode("[metaslider id=29]"); ?>
				<!-- <img src="//192.168.1.35:3002/wp-content/uploads/2017/10/quay-reception-1-1200x450.jpg" alt="" class="acf-image"> -->
				<?php get_template_part( 'inc/feature-slider' ); ?>
			</div>
			<?php  if(get_field('slider_text')) : ?>
				<div class="slider-text">
					<?php
						echo get_field('slider_text');
					?>
				</div>
			<?php endif; ?>
			<!-- <div id="leadform-home">
				<?php //echo do_shortcode('[contact-form-7 id="1392" title="Book a Consultation - Front Page"]'); ?>
			</div> -->

		</section>
	</div>


	<?php else : ?>
		<section class="subimage"> <?php echo do_shortcode("[metaslider id=$metasliderID]"); ?> </section>
	<?php endif; ?>

