
<?php if( have_rows('feature_sliders') ): ?>
   <section class="feature-slider mb-lg">
        <?php while ( have_rows('feature_sliders') ) : the_row(); ?>
            <div class="slick-container">
                <?php echo wp_get_attachment_image( get_sub_field('feature_slider_image'), 'full-size' ); ?>
            </div>
        <?php endwhile; ?>
    </section>
<?php endif; ?>


<script>
	jQuery(document).ready(function(){
	  jQuery('.feature-slider').slick({
			lazyLoad       : 'ondemand',
			infinite       : true,
			autoplay       : true,
			slidesToShow   : 1,
			slidesToScroll : 1,
			speed          : 1000,
			fade           : true,
			cssEase        : 'linear',
			arrows         : false,
			dots           : false,
	  });
	});	
</script>