<?php
/**
 * The Sidebar containing the primary and secondary widget areas.
 *
 * @package WordPress
 * @subpackage Twenty_Ten
 * @since Twenty Ten 1.0
 */
?>

		<aside role="complementary">
<?php
			
  if ( is_single() || is_archive() || is_home() || is_author() || is_tag() || is_category() ) {
  dynamic_sidebar( 'blog-sidebar' );

} else {
  dynamic_sidebar( 'primary-widget-area' );
}
?>
		</aside>
		
